#+title: Building Blocks

* List of Building Blocks required for Hashtables
- Arrays
- Operations supported for arrays
   - Insert at given index
   - Search for an element at a given index
   - Change value at a given index
