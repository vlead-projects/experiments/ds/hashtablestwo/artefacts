#+TITLE: Inserting in Hash table
#+AUTHOR: VLEAD
#+DATE: [2018-06-12 Tue]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
* Global variables
#+NAME: part-1
#+BEGIN_SRC js
var allCorrect5 = 1;
var statuslp = [];
var statusqp = [];
#+END_SRC
* Function to check
#+NAME: part-2
#+BEGIN_SRC 
var check2 = function() {

	var valueslp = [];
	var valuesqp = [];
	for(i=0; i<20; i++){
		valueslp[i] = document.getElementById("lp" + i).value;
		valuesqp[i] = document.getElementById("qp" + i).value;
	}

	var answerslp = ["", "", "", "", "44", "64", "86", "27", "", "", "", "", "32", "33", "34", "35", "36", "52", "", ""];
	var answersqp = ["", "52", "", "", "44", "64", "86", "27", "", "", "", "", "32", "33", "34", "35", "36", "", "", ""];

	for(i=0; i<20; i++){
		if(valueslp[i] == answerslp[i])
			statuslp[i] = 1;
		else
			statuslp[i] = 0;
		if(valuesqp[i] == answersqp[i])
			statusqp[i] = 1;
		else
			statusqp[i] = 0;
	}

	allCorrect5 = 1;

	for(i=0; i<20; i++){
		var templp = document.getElementById("lp" + i);
		var tempqp = document.getElementById("qp" + i);
		// if user's answer is correct, box becomes green
		if(statuslp[i]){
			templp.style.background = "#9DFF55";
		}
		// if user's answer is wrong, box becomes red
		else{
			allCorrect5 = 0;
			templp.style.background = "#FFB273";	
		}
		// if user's answer is correct, box becomes green
		if(statusqp[i]){
			tempqp.style.background = "#9DFF55";
		}
		// if user's answer is wrong, box becomes red
		else{
			allCorrect5 = 0;
			tempqp.style.background = "#FFB273";	
		}
	}

	// display this only after all answers are correct
	if(allCorrect5){
		document.getElementById("collision").style.display = "block";
		document.getElementById("collision").style.maxWidth= "700px";
	}

	document.getElementById("legend5").style.display = "block";
}
#+END_SRC
* Function to reset
#+NAME: part-3
#+BEGIN_SRC 
var restart2 = function(){
	for(let i=0; i<20; i++){
		statuslp[i] = 0;
		statusqp[i] = 0;
		document.getElementById("lp" + i).style.background = "white";
		document.getElementById("qp" + i).style.background = "white";
		document.getElementById("lp" + i).value = "";
		document.getElementById("qp" + i).value = "";
	}
	document.getElementById("legend5").style.display = "none";
}
#+END_SRC

* Tangle							:boilerplate:
** sources
#+BEGIN_SRC js :tangle js/source5.js :eval no :noweb yes
<<part-1>>
<<part-2>>
<<part-3>>
#+END_SRC

